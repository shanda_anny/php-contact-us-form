# Project Symfony-form

[![Build Status](https://gitlab.com/shanda_anny/php-contact-us-form/badges/master/build.svg)](https://gitlab.com/shanda_anny/php-contact-us-form/pipelines)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Description

It's a PHP project with Symfony framework. Here no table is required to submit the contact us form.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

### Prerequisites

- PHP (>=7.4) with extensions (e.g., PDO, intl, mbstring)
- Composer (https://getcomposer.org/)
- Symfony (https://symfony.com/)

### Clone the Repository

Clone this repository to your local machine:

git clone https://gitlab.com/shanda_anny/php-contact-us-form


### Install Dependencies

Navigate to the project directory and install the required dependencies:

cd php-contact-us-form
composer install


### Set Up the Database

Make sure you have a database configured for the application and update the `.env` file with your database credentials.

# .env
DATABASE_URL=mysql://db_user:db_password@localhost:3306/db_name


### Create the Database Tables

Run the following command to create the required database tables:

php bin/console doctrine:database:create

N.B.: It's for symfony project not for the form submission.


### Start the Development Server

You can use Symfony's built-in web server to run the application:

symfony serve
or
symfony server:start

The application will be available at `http://localhost:8000/`.

## Usage

The project is for submitting those form which data is not necessarily required to be stored in the database. Though here the data will be stored in the session and send the mails. Then will be removed for users and admin.

## Contributing

Contributions are welcome! If you'd like to contribute to the project, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and commit them.
4. Push your branch to your forked repository.
5. Open a pull request on the main repository.

## License

This project is open-source and available under the [MIT License](https://opensource.org/licenses/MIT).
