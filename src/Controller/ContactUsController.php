<?php

namespace App\Controller;

use App\Form\ContactUsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ContactUsController extends AbstractController
{
    #[Route('/', name: 'contact_us')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(ContactUsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Process the form data (you can do anything with the data, like sending an email).

            // For example, you can access the form data like this:
            $formData = $form->getData();
            // Now $formData contains an array with the form fields and their values.
            // You can use this data as per your requirements.

            // Extract the first name from the form data
            $firstName = $formData['firstName'];
            $lastName = $formData['lastName'];
            $email = $formData['email'];
            $phoneNumber = $formData['phoneNumber'];
            $subject = $formData['subject'];
            $message = $formData['message'];

            // Store the first name in the session
            $request->getSession()->set('first_name', $firstName);
            $request->getSession()->set('last_name', $lastName);
            $request->getSession()->set('email', $email);
            $request->getSession()->set('phone_number', $phoneNumber);
            $request->getSession()->set('subject', $subject);
            $request->getSession()->set('message', $message);

            // You can also add flash messages or redirect the user to a success page.

            // return $this->redirectToRoute('success', [
            //     'first_name' => $firstName,
            // ]); // Redirect to a success page.
            return $this->redirectToRoute('success');
        }
        return $this->render('contact_us/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/success', name: 'success')]
    public function successPage(Request $request, MailerInterface $mailer): Response
    {
        // Check if the session data for the form fields exists
        if ($request->getSession()->has('first_name')) {
            // Get the form field data from the session
            $firstName = $request->getSession()->get('first_name');
            $lastName = $request->getSession()->get('last_name');
            $email = $request->getSession()->get('email');
            $phoneNumber = $request->getSession()->get('phone_number');
            $subject = $request->getSession()->get('subject');
            $message = $request->getSession()->get('message');

            // Clear the session data to avoid showing the data on subsequent visits
            $request->getSession()->remove('first_name');
            $request->getSession()->remove('last_name');
            $request->getSession()->remove('email');
            $request->getSession()->remove('phone_number');
            $request->getSession()->remove('subject');
            $request->getSession()->remove('message');

            // Display a flash message indicating a successful form submission
            $this->addFlash('success', 'Thank you for your submission!');

            // Send email to the admin
            $adminEmail = 'admin@example.com'; // Replace with your admin email address
            $this->sendAdminEmail($mailer, $adminEmail, $firstName, $lastName, $email, $subject, $message);

            // Send email to the user who submitted the form
            $userEmail = $email; // Assuming the email is submitted in the form
            $this->sendUserEmail($mailer, $userEmail, $firstName, $subject, $message);

            return $this->render('contact_us/success.html.twig', [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'phone_number' => $phoneNumber,
                'subject' => $subject,
                'message' => $message,
            ]);
        } else {
            // Redirect to the Fail page when the session data for the form fields does not exist
            return $this->redirectToRoute('fail');
        }
    }




    #[Route('/fail', name: 'fail')]
    public function failPage(): Response
    {
        return $this->render('contact_us/fail.html.twig');
    }


    private function sendAdminEmail(MailerInterface $mailer, string $adminEmail, string $firstName, string $lastName, string $email, string $subject, string $message): void
    {
        $email = (new Email())
            ->from($email)
            ->to($adminEmail)
            ->subject('New Form Submission')
            ->html(sprintf(
                'New form submission:<br>First Name: %s<br>Last Name: %s<br>Email: %s<br>Subject: %s<br>Message: %s',
                $firstName,
                $lastName,
                $email,
                $subject,
                $message
            ));

        $mailer->send($email);
    }

    private function sendUserEmail(MailerInterface $mailer, string $userEmail, string $firstName, string $subject, string $message): void
    {
        $email = (new Email())
            ->from('noreply@example.com') // Replace with your noreply email address
            ->to($userEmail)
            ->subject('Thank You for Your Submission')
            ->html(sprintf(
                'Dear %s,<br><br>Thank you for your form submission.<br>Subject: %s<br>Message: %s',
                $firstName,
                $subject,
                $message
            ));

        $mailer->send($email);
    }
}
